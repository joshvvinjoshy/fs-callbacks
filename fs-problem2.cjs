const fs = require('fs');
// const path = require('path');
function convert(fpath){
    fs.readFile(fpath,'utf-8', (err,data)=>{
        if(err){
            console.error(err);
        }
        else{
            const data_up = data.toUpperCase();
            const pathh = '/home/joshvvinjoshy/mb/fscallbacks/';
            // console.log(data_up);
            fs.writeFile(pathh+'lipsum_upper.txt', data_up, (err)=>{
                if(err){
                    console.error(err);
                }
                else{
                    fs.appendFile(pathh+'filenames.txt', 'lipsum_upper.txt', (err)=>{
                        if(err){
                            console.error(err);
                        }
                        else{
                            console.log('appended lipsum_upper.txt to filenames.txt');
                        }
                    })

                    fs.readFile(pathh + 'lipsum_upper.txt', 'utf-8', (err,data)=>{
                        if(err){
                            console.error(err);
                        }
                        else{
                            const data_lower = data.toLowerCase();
                            const sentenceRegex = /[^.!?]+[.!?]+/g;
                            const sentences = data_lower.match(sentenceRegex);
                            // console.log(sentences);
                            const data_sentences = sentences.map(function (sentence){ 
                                                                return sentence.trim();
                                                            });
                            // console.log(data_sentences);
                            const data_sentences_str = data_sentences.toString();
                            fs.writeFile(pathh +'lipsum_sentence.txt', data_sentences_str, (err)=>{
                                if(err){
                                    console.error(err);
                                }
                                else{
                                    fs.appendFile(pathh+'filenames.txt', ',lipsum_sentence.txt', (err)=>{
                                        if(err){
                                            console.error(err);
                                        }
                                        else{
                                            console.log('appended lipsum_sentence.txt to filenames.txt');

                                        }
                                    })
                                    fs.readFile(pathh +'lipsum_sentence.txt', 'utf-8', (err,data)=>{
                                        if(err){
                                            console.error(err);
                                        }
                                        else{
                                            const data_list = data.split(',');
                                            data_list.sort();
                                            const sorted_data_list = data_list.join('\n');
                                            fs.writeFile(pathh + 'sorted_lipsum_sentence.txt', sorted_data_list, (err)=>{
                                                if(err){
                                                    console.error(err);
                                                }
                                                else{
                                                    fs.appendFile(pathh + 'filenames.txt', ',sorted_lipsum_sentence.txt', (err)=>{
                                                        if(err){
                                                            console.error(err);
                                                        }
                                                        else{
                                                            console.log('appended sorted_lipsum_sentence.txt to filenames.txt');
                                                            fs.readFile(pathh+'filenames.txt', 'utf-8', (err,data)=>{
                                                                if(err){
                                                                    console.error(err);
                                                                }
                                                                else{
                                                                    const arr = data.split(',');
                                                                    
                                                                    arr.map((filename)=>{
                                                                        fs.rm(pathh + filename, (err)=>{
                                                                            if(err){
                                                                                console.error(err);
                                                                            }
                                                                            else{
                                                                                console.log(filename,'is removed successfully');
                                                                            }
                                                                        })
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                   
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}
module.exports = convert;
const fs = require('fs');
const path = require('path');
function deleteFile(files , randomNumberOfFiles, cb){;
    let count = 0;
    for(const file in files){
        const filepath = files[file];
        fs.unlink(filepath, (err) =>{
            if(err){
                console.error(err);
            }
            else{
                count ++;
                console.log('deleted the json file', count);
                if(count == randomNumberOfFiles){
                    cb(null);
                }                            
            }
        })  
    }          
}
function createFile(absolutePathOfRandomDirectory, randomNumberOfFiles, cb){
    let cur_no_of_files=0;
    let files = [];
    for(let ind = 0; ind < randomNumberOfFiles; ind ++){
        const randdata = {
            id : ind,
            age : ind+1
        };
        const jsonData = JSON.stringify(randdata, null, 2);
        const filename = randdata['id'] +'.json';
        const filepath = path.join(absolutePathOfRandomDirectory, filename);
        fs.writeFile(filepath, jsonData, (err) =>{
            if(err){
                console.error(err);
            }
            else{                  
                cur_no_of_files ++;
                files.push(filepath);
                console.log('json file created successfully', cur_no_of_files);    
                if(cur_no_of_files == randomNumberOfFiles){
                    cb(null, files);          
                }
            }             
        });
    }
}
function fsProblem1(absolutePathofRandomDirectory, randomNumberOfFiles=1){
    fs.mkdir(absolutePathofRandomDirectory, (err) =>{
        if(err){
            console.error(err);
        }
        else{
            console.log('Directory created in the given path' );
            createFile(absolutePathofRandomDirectory, randomNumberOfFiles, (err, files)=>{
                if(err){
                    console.error(err);
                }
                else{
                    deleteFile(files, randomNumberOfFiles, (err)=>{
                        if(err){
                            console.error(err);
                        }
                        else{
                            console.log('all files deleted');
                            fs.rmdir(absolutePathofRandomDirectory, (err)=>{
                                if(err){
                                    console.error(err);
                                }
                                else{
                                    console.log('directory deleted');
                                }
                            })
                        }
                    })
                }
            });
        }       
    });
            
}

module.exports = fsProblem1;